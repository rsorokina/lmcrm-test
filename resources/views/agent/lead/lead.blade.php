<table class="table">
    <tr>
        <td>icon</td>
        <td></td>
    </tr>
    <tr>
        <td>date</td>
        <td>{{$lead->date}}</td>
    </tr>
    <tr>
        <td>name</td>
        <td>{{$lead->name}}</td>
    </tr>
    <tr>
        <td>phone</td>
        <td>{{$lead->phone->phone}}</td>
    </tr>
    <tr>
        <td>phone</td>
        <td>{{$lead->email}}</td>
    </tr>
    @foreach($lead->sphereAttr as $sphereAttr)
    <tr>
        <td>{{$sphereAttr->label}}</td>
        <td>
        @foreach($sphereAttr->spheremasks($lead->id) as $masks)
            @foreach($sphereAttr->options as $options)        
                @if ($masks->mask($options->mask()) == 1)
                    {{$options->value}}
                @endif
            @endforeach
        @endforeach
        </td>
    </tr>
    @endforeach
</table>