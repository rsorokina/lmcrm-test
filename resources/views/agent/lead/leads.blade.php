@extends('layouts.master')
{{-- Content --}}
@section('content')
    <table class="table leads">
        <thead>
            <tr>
                <th>Icon</th>
                <th>Date</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
            @foreach($openleads as $openlead)
            <tr data-id="{{$openlead->lead->id}}">
                <td></td>
                <td>{{$openlead->lead->date}}</td>
                <td>{{$openlead->lead->name}}</td>
                <td>{{$openlead->lead->phone->phone}}</td>
                <td>{{$openlead->lead->email}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
@stop